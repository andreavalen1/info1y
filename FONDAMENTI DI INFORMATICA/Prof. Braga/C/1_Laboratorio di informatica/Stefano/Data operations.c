#include <stdio.h>
#include <string.h>
#include <math.h>

typedef struct {
		int giorno;
		int mese;
		int anno;
	} data;
	
int bisestile(int anno);
int valida(data one);
int distanza(data one, data two);
data aggiungi(data one, int giorni);
void stampa_date(data one);

int main(){
	
	char t;
	int i, k;
	data one, two, nuova;
	printf("----Data operations----\n");
	printf("Available operations: \n");
	printf("1) Data range (in days)\n");
	printf("2) Add days\n");
	printf("Select operations (1 or 2), write unother character to exit: ");
	scanf("%c", &t);
	for(; (t=='1' || t=='2');){
		if(t=='1'){
			printf("Write the day (of the first date): ");
			scanf("%d", &one.giorno);
			printf("Write the month (of the first date): ");
			scanf("%d", &one.mese);
			printf("Write the year (of the first date): ");
			scanf("%d", &one.anno);
			printf("Write the day (of the second date): ");
			scanf("%d", &two.giorno);
			printf("Write the month (of the second date): ");
			scanf("%d", &two.mese);
			printf("Write the year (of the second date): ");
			scanf("%d", &two.anno);
			printf("First date: ");
			stampa_date(one);
			printf("Second date: ");
			stampa_date(two);
			printf("The range is (in days): %d\n", distanza(one, two));
			printf("Select operations (1 or 2), write unother character to exit: ");
			fflush(stdin);
			scanf("%c", &t);
			fflush(stdin);
		}
		else if(t=='2'){
			printf("Write the day: ");
			scanf("%d", &one.giorno);
			printf("Write the month: ");
			scanf("%d", &one.mese);
			printf("Write the year: ");
			scanf("%d", &one.anno);
			printf("Write how many days to add: ");
			scanf("%d", &k);
			nuova=aggiungi(one, k);
			printf("First date: ");
			stampa_date(one);
			printf("New date: ");
			stampa_date(nuova);
			printf("If you want to keep select operations (1 or 2), instead write unother character to exit: ");
			fflush(stdin);
			scanf("%c", &t);
			fflush(stdin);
		}
	}
	
	return 0;
}

void stampa_date(data one){
	printf("%d/%d/%d\n", one.giorno, one.mese, one.anno);
}

data aggiungi(data one, int giorni){
	for(; giorni>0;){
		one.giorno++;
		if(valida(one)){
			giorni--;
		}
		else{
			one.giorno=1;
			one.mese++;
			if(valida(one)){
				giorni--;
			}
			else{
				one.giorno=1;
				one.mese=1;
				one.anno++;
				giorni--;
			}
		}
	}
	return one;
}

int distanza(data one, data two){
	int d=0;
	data temp;
	if((one.anno>two.anno) || ((one.anno==two.anno) && (one.mese>two.mese)) || ((one.anno==two.anno && one.mese==two.mese) && (one.giorno>two.giorno))){
		temp=one;
		one=two;
		two=temp;
	}
	for(; (one.anno<two.anno || (one.anno==two.anno && one.mese<two.mese) || (one.anno==two.anno && one.mese==two.mese && one.giorno<two.giorno)); ){
		one.giorno++;
		if(valida(one)){
			d++;
		}
		else{
			one.giorno=1;
			one.mese++;
			if(valida(one)){
				d++;
			}
			else{
				one.giorno=1;
				one.mese=1;
				one.anno++;
				d++;
			}
		}
	}
	return d;
}

int valida(data one){
	if(one.anno==1582){		//caso particolare (anno 1582)
		if(((one.mese==1 || one.mese==3 || one.mese==5 || one.mese==7 || one.mese==8 || one.mese==12) && (one.giorno>=1 && one.giorno<=31)) || ((one.mese==4 || one.mese==6 || one.mese==9 || one.mese==11) && (one.giorno>=1 && one.giorno<=30)) || (one.mese==2 && (one.giorno>=1 && one.giorno<=28)) || (one.mese==10 && ((one.giorno>=1 && one.giorno<5) || (one.giorno>14 && one.giorno<=31)))){
			return 1;
		}
		return 0;
	}
	if(bisestile(one.anno)){	//se � bisestile
		if(((one.mese==1 || one.mese==3 || one.mese==5 || one.mese==7 || one.mese==8 || one.mese==10 || one.mese==12) && (one.giorno>=1 && one.giorno<=31)) || ((one.mese==4 || one.mese==6 || one.mese==9 || one.mese==11) && (one.giorno>=1 && one.giorno<=30)) || (one.mese==2 && (one.giorno>=1 && one.giorno<=29))){
			return 1;	
		}
	}
	else if(bisestile(one.anno)==0){	//se non � bisestile
		if(((one.mese==1 || one.mese==3 || one.mese==5 || one.mese==7 || one.mese==8 || one.mese==10 || one.mese==12) && (one.giorno>=1 && one.giorno<=31)) || ((one.mese==4 || one.mese==6 || one.mese==9 || one.mese==11) && (one.giorno>=1 && one.giorno<=30)) || (one.mese==2 && (one.giorno>=1 && one.giorno<=28))){
			return 1;
		}
	}
	return 0;
}

int bisestile(int anno){
	if((anno>=1582 && anno%4==0) && (anno%100!=0 || (anno%100==0 && anno%400==0))){
		return 1; 	//vero
	}
	return 0;
}
