#include <stdio.h>
#include <string.h>
#include <math.h>

#define LUNG_CHAR 30
#define LUNG_MAX 130
int main(){
	
	char par1[LUNG_CHAR], par2[LUNG_CHAR], c;
	int alpha[LUNG_MAX] = {0};
	int beta[LUNG_MAX] = {0};
	
	printf("---Anagrammi---\n");
	printf("Inserire due parole (verifichero' se sono anagrammi): ");
	scanf("%s", par1);
	scanf("%s", par2);
	
	int i=0;
	int ps=0;
	if(strlen(par1)==strlen(par2)){
		for(i=0; i<strlen(par1); i++){	//composizione di alpha (metodo Braga)
			c=par1[i];
			alpha[c]++;
		}
		for(i=0; i<strlen(par2); i++){	//composizione di Beta (metodo Braga)
			c=par2[i];
			beta[c]++;
		}
		for(i=0; i<LUNG_MAX; i++){		//comparazione stringhe (anagrammi)
			if(alpha[i]!=beta[i]){
				ps=2;
				i=LUNG_MAX;
			}
		}
	}
	
	
	if(strlen(par1)!=strlen(par2) || ps==2){
		printf("\nLe parole inserite non sono anagrammi");
	}
	else
		printf("\nLe parole inserite sono anagrammi");
	
	return 0;
}
