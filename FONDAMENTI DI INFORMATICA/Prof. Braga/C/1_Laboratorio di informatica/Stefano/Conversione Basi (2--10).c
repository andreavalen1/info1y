#include <stdio.h>
#include <math.h>


int main(){
	
	int  BASE=0, i=0, dec=0, baseo=0, n=0, w=0, t=0;
	int bin[32] = {0};
	char c;
	char array[32] = {0};
	
	printf("--Conversione Bin-Dec (naturale)--\n");
	printf("Inserisci la base del numero di partenza: ");
	scanf("%d", &BASE);
	fflush(stdin);
	printf("Inserisci la base in uscita: ");
	scanf("%d", &baseo);
	fflush(stdin);
	printf("Inserisci un numero in base %d (max 32 cifre): ", BASE);
	
	
	i=-1;
	c=0;
	scanf("%c", &c);
	for(; c>=48 && c<=47+(BASE);){
		i++;
		array[i]=c-48;
		scanf("%c", &c);
		w=1;
	}
	
	
	n=BASE;
	dec=0;
	while(i>=0){
			dec=dec+(array[i]*1);
		if(i>=1){
			i--;
			dec=dec+(array[i]*BASE);
			i--;
		}
		while(i>=0){
			n=n*BASE;
			dec=dec+(array[i]*n);
			i--;
		}
	}
	
		if(dec>=0){
		for(t=0; dec!=0; t++){     //quando il numero � positivo
			bin[t]=dec%baseo;
			dec=dec/baseo;
		}
	}	
	
	t--;
	
	printf("Corrispondente numero in base %d = ", baseo);
	for(; t>=0; t--){
		printf("%d", bin[t]);
	}
	
//	if(w==1){
//		printf("Conversione in Dec = %d", dec);
//	}
//	if(w==0){
//		printf("Non hai inserito nessun numero");
//	}
	
	return 0;
}
