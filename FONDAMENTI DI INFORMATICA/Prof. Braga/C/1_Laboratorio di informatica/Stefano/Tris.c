#include <stdio.h>
#include <string.h>
#include <math.h>

void stampa(char griglia[][3]);
int conta_vuote(char griglia[][3]);
void inizializza(char griglia[][3]);
void mossa(char griglia[][3], char nome[], char symbol);
int controlla_vincitore(char griglia[][3], char symbol1);

int main(){
	char player1[20]="", player2[20]="";
	char griglia[3][3];
	
	
	printf("-----Tic Tac Toe-----\n");
	printf("Scrivere il nome del player1: ");
	scanf("%s", player1);
	printf("Scrivere il nome del player2: ");
	scanf("%s", player2);
	
	
	inizializza(griglia);
	stampa(griglia);
	for(; ; ){
		mossa(griglia, player1, 'x');
		stampa(griglia);
		if(controlla_vincitore(griglia, 'x')){
			printf("\n%s ha vinto!!!\n", player1);
			break;
		}
		if(conta_vuote(griglia)==0){
			printf("Partita terminata. Nessun vincitore");
			break;
		}
		mossa(griglia, player2, '0');
		stampa(griglia);
		if(controlla_vincitore(griglia, '0')){
			printf("\n%s ha vinto!!!\n", player2);
			break;
		}
		if(conta_vuote(griglia)==0){
			printf("Partita terminata. Nessun vincitore");
			break;
		}
	}
	
	
	return 0;
}

int conta_vuote(char griglia[][3]){
	int i, j, counter=0;
	for(i=0; i<3; i++){
		for(j=0; j<3; j++){
			if(griglia[i][j]=='\0'){
				counter++;
			}
		}
	}
	return counter;
}

int controlla_vincitore(char griglia[][3], char symbol1){
	int i, j;
	for(i=0; i<3; i++){
		if((griglia[i][0]==symbol1 && griglia[i][1]==symbol1 && griglia[i][2]==symbol1)){
			return 1;
		}
	}
	for(j=0; j<3; j++){
		if((griglia[0][j]==symbol1 && griglia[1][j]==symbol1 && griglia[2][j]==symbol1)){
			return 1;
		}
	}
	if((griglia[0][0]==symbol1 && griglia[1][1]==symbol1 && griglia[2][2]==symbol1)){
		return 1;
	}
	if((griglia[0][2]==symbol1 && griglia[1][1]==symbol1 && griglia[2][0]==symbol1)){
		return 1;
	}
	return 0;
}

void mossa(char griglia[][3], char nome[], char symbol){
	int x, y, i;
	printf("Mossa di %s [x y] : ", nome);
	scanf("%d", &x);
	scanf("%d", &y);
	for(i=0; i==0; )
		if(((x==0 || x==1 ||x==2) && (y==0 || y==1 || y==2)) && (griglia[x][y]=='\0')){
			griglia[x][y]=symbol;
			i=1;
		}
		else{
			printf("Mossa non valida. Ripeti la mossa [x y] : ");
			scanf("%d", &x);
			scanf("%d", &y);
		}
}

void inizializza(char griglia[][3]){
	int i, j;
	for(i=0; i<3; i++){
		for(j=0; j<3; j++){
			griglia[i][j]='\0';
		}
	}
}

void stampa(char griglia[][3]){
	int i;
	printf("  \\ y\n");
	printf(" x \\   0   1   2\n");
	printf("    \\------------\n");
	for(i=0; i<3; i++){
		printf(" %d  | %c | %c | %c |\n", i, griglia[i][0], griglia[i][1], griglia[i][2]);
		printf("    -------------\n");
	}
}
