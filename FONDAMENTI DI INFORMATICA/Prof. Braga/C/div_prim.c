#include <stdio.h>

void swap(int *n1, int *n2){
	
   	int t;
 
   	t  = *n1;
  	*n1 = *n2;
   	*n2 = t;
   
}

int main() {
	
	int num1,num2;
	int	mcd = 1;
	
	scanf("%d%d",&num1,&num2);
	
	//printf("\n%d num1\n%d num2\n\n\n",num1,num2);
	
	if(num2<num1)
		swap(&num2,&num1);
	
	//printf("\n%d num1\n%d num2\n\n\n",num1,num2);
	
	for(int i=2; i<=num1; i++){
		
		if(num1%i==0 && num2%i==0){
			
			mcd = i;

			printf("\n%d e' divisore comune\n",i);
			
		}
		
		
	}
	
	printf("\n");
	
	if(mcd==1)
		printf("%d e %d sono primi tra loro\n",num1,num2);
	else
		printf("%d e %d NON sono primi tra loro\n",num1,num2);
	
	return 0;
	
}