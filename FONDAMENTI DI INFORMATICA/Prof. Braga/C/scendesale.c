#include <stdio.h>

void effe0(int a){							//EFFE 0
    
    for(int i = a; i > 0; --i)
	{
        for(int j = 0; j < i; ++j)
		{
            printf("%d",i);
        }
    }
	
    printf(".");
	
    for(int i = 1; i <= a; ++i)
	{
        for(int j = 0; j < i; ++j)
		{
            printf("%d",i);
        }
    }
	
	printf("\n");
}

void effe1(int a){							//EFFE 1
    
    for(int i = 1; i <= a ; ++i)
	{
        for(int j = a; j >= i  ; j--)
		{
            printf("%d",i);
        }
    }
	
    printf(".");
	
    for(int i = a; i > 0; i--)
	{
        for(int j = 0; j <= (a-i); j++)
		{
            printf("%d",i);
        }
    }
	
	printf("\n");
}

void effe2(int a){							//EFFE 2
    
    for(int i = a; i > 0; --i)
	{
        for(int j = 0; j < i; ++j)
		{
            printf("%d",i);
        }
    }
	
    for(int i = 2; i <= a; ++i)
	{
        for(int j = 0; j < i; ++j)
		{
            printf("%d",i);
        }
    }
	
	printf("\n");
}
	
void effe3(int a){							//EFFE 3
    
    for(int i = 1; i <= a ; ++i)
	{
        for(int j = a; j >= i  ; j--)
		{
            printf("%d",i);
        }
    }
	
    for(int i = a-1; i > 0; i--)
	{
        for(int j = 0; j <= (a-i); j++)
		{
            printf("%d",i);
        }
    }
	
	printf("\n");
}

void effe4(int a){							//EFFE 4
	
    for(int i = 2; i <= a; ++i)
	{
        for(int j = 0; j < i; ++j)
		{
            printf("%d",i);
        }
    }
	
	for(int i = a-1; i > 0; --i)
	{
        for(int j = 0; j < i; ++j)
		{
            printf("%d",i);
        }
    }
	
	printf("\n");
}

void effe5(int a){							//EFFE 5
	
    for(int i = a; i > 0; i--)
	{
        for(int j = 0; j < (a-i+1); j++)
		{
            printf("%d",i);
        }
    }
	
	for(int i = 2; i <= a ; ++i)
	{
        for(int j = a; j >= i  ; j--)
		{
            printf("%d",i);
        }
    }
	
	printf("\n");
}

int main (void)
{
	int num,fun;
	
	printf("Insert number: ");
	scanf("%d",&num);
	
	printf("Inserst function (0 to 5): ");
	scanf("%d",&fun);
	
	switch (fun)
	{
		
		case 0:
			effe0(num);
			break;
		case 1:
			effe1(num);
			break;
		case 2:
			effe2(num);
			break;
		case 3:
			effe3(num);
			break;
		case 4:
			effe4(num);
			break;
		case 5:
			effe5(num);
			break;
			
		default:
			printf("ERROR, INPUT NOT VALID\n");
			break;	
		
	}
	
	return 0;
}