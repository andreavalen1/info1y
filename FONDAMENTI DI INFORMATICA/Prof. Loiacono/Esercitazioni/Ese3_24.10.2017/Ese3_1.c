/*

Scrivere un programma che chieda all'utente di inserire due sequenze di N numeri reali (e le memorizza in un array) ed esegua le seguenti
operazioni:
- Dica quale dei due array ha valore medio più alto
- Calcoli l'array concatenato tra i due array in ingresso
- Trovi il massimo ed il minimo tra tutti i valori inseriti, indicando in quale dei due array sono presenti i due valori di massimo e minimo
- Stampi a video il loro prodotto scalare: [a b c d e] * [f g h i l]' = af + bg + ch + di + el

*/

#include <stdio.h>
#define N 100

int main()
{

    float array1[N], array2[N], arrayC[N*2], prodottoS;
    int i = 0, m_1 = 0, m_2 = 0, imin, imax;

    printf("Inserisci i valori del primo array (0 per terminare):\n");
    for (i = 0; i < N; i++) {
        scanf("%f%*c",&array1[i]);
    }

    printf("Inserisci i valori del secondo array (0 per terminare):\n");
    for (i = 0; i < N; i++) {
        scanf("%f%*c",&array2[i]);
    }

    // PRIMO PUNTO
    for (i=0; i<N; i++ ) {
        m_1 = m_1 + array1[i];
        m_2 = m_2 + array2[i];
    }

    if(m_1/N > m_2/N)
        printf("Valore medio dell'array1 MAGGIORE di quello dell'array2\n");
    else if (m_1/N == m_2/N)
        printf("Valore medio dell'array1 UGUALE a quello dell'array2\n");
    else
        printf("Valore medio dell'array2 MAGGIORE di quello dell'array1\n");
    // FINE PRIMO PUNTO

    // SECONDO PUNTO
    for (i=0; i<N; i++) {
        arrayC[i] = array1[i];
        arrayC[i+N] = array2[i];
    }
    // FINE SECONDO PUNTO

    // TEZO PUNTO
    imin = 0;
    imax = 0;

    for(i=1; i < N*2; i++) {
        if(arrayC[i] < arrayC[imin])
            imin = i;
        if(arrayC[i] > arrayC[imax])
            imax = i;
    }

    printf("Il valore minimo è %f ", arrayC[imin]);
    if(imin < N)
        printf("e si trova in array1\n");
    else
        printf("e si trova in array2\n");
    // FINE TERZO PUNTO

    // QUARTO PUNTO
    prodottoS = 0;

    for (i=0; i<N; i++) {
        prodottoS = prodottoS + (array1[i] * array2[i]);
    }

    printf("Il prodotto scalare tra i due array è %f\n", prodottoS);
    // FINE QUARTO PUNTO

    return 0;
}
