#include <stdio.h>
#include <math.h>

int verificaLati(float latoA, float LatoB, float LatoC);

int main()
{
    float latoA, latoB, latoC;
    int verifica;
    printf("Inserire i valori dei lati:\n");
    scanf("%f%f%f%*c", &latoA, &latoB, &latoC);
    /*printf("Inserire il valore del seconda lato:");
    scanf("%f%*c", &latoB);
    printf("Inserire il valore del terzo lato:");
    scanf("%f%*c", &latoC);*/

    verifica = verificaLati(latoA, latoB, latoC);

    if(verifica == 3) {
        float s, area;
        s = (latoA+latoB+latoC)/2;
        area = sqrtf(s*(s-latoA)*(s-latoB)*(s-latoC));
        printf("L'area di materiale richiesta è %f\n", area);
        printf("La lunghezza di materiale protettivo richiesto è: %f\n", s*2);
    } else {
        printf("I lati inseriti non corrispondono a quelli di un triangolo\n");
    }

    return 0;

}

int verificaLati(float latoA, float latoB, float latoC)
{
    int risultato = 0;

    if((latoA < latoB+latoC) && (latoA>fabs(latoB-latoC)))
        risultato++;
    if((latoB < latoA+latoC) && (latoB>fabs(latoA-latoC)))
        risultato++;
    if((latoC < latoA+latoB) && (latoC>fabs(latoA-latoB)))
        risultato++;

    return risultato;
}
